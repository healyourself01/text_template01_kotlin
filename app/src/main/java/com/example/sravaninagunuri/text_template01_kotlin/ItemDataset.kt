package com.example.sravaninagunuri.text_template01_kotlin

import android.util.Log
import com.example.sravaninagunuri.text_template01_kotlin.admob.AdmobUtility
import java.io.File


object ItemDataset {
    var position:Int = 0
    var topics: ArrayList<Topic>
        get() = kotlin.run {
            try {
                mDbHelper.getTopics()
            } catch (ex:UninitializedPropertyAccessException){
                Log.e("DATABASE","Property not initialized.")
            } } as ArrayList<Topic>
        set(value) = TODO()

    var menus: ArrayList<Menu>
        get() = kotlin.run {
            try {
                mDbHelper.getMenus()
            } catch (ex:UninitializedPropertyAccessException){
                Log.e("DATABASE","Property not initialized.")
            } } as ArrayList<Menu>
        set(value) = TODO()

    var currentMenuTitle: String
        get() = kotlin.run {
            menus.elementAt(position).MENU_TITLE
        } as String
        set(value) = TODO()


    var itemText: String
        get() = kotlin.run {
            try {
                mDbHelper.getItem()
            } catch (ex:UninitializedPropertyAccessException){
                Log.e("DATABASE","Property not initialized.")
            } } as String
        set(value) = TODO()
    var position_bookmark:Int = 0
    var topics_icons = Images_Topics.topic_icons
    lateinit var APP_DIR:File

    lateinit var items:Collection<Item>
    lateinit var items2:ArrayList<Item>

    var TOPIC_ID:Int = 1
    var MENU_ID:Int = 1
    lateinit var mDbHelper: DataBaseHelper

    var ITEM_TEXT =  ""
    lateinit var item_current:Item

    lateinit var admob: AdmobUtility
    const val APP_JSON_FILE_NAME = "app_data_full.json"
    const val ASSET_URI = "file:///android_asset/app_images/"

}