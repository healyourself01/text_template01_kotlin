package com.example.sravaninagunuri.text_template01_kotlin

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.sravaninagunuri.text_template01_kotlin.admob.AdObject
import com.example.sravaninagunuri.text_template01_kotlin.fragments.*
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.gson.Gson
import org.jetbrains.anko.*
import com.google.gson.reflect.TypeToken
import java.io.File
import java.lang.Math.ceil


class MainActivity : AppCompatActivity(), AppInterfaces {


private val START_SCREEN ="START_SCREEN"
private val TOPICS ="TOPICS"
private val MENUS = "MENUS"
private val ITEM = "ITEM"
private val BOOKMARK_MENU="BOOKMARK_MENU"
private val BOOKMARK_ITEM="BOOKMARK_ITEM"


private var BANNER_LOADED = false
private val DB_NAME =  "db_vantalu.db" //CHANGE THE DB NAME FOR EVERY APP

var images_all:ArrayList<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        images_all.addAll(assets.list("app_images").filterNotNull() )

        // Copy database
        val assetDatabaseOpenHelper = AssetDatabaseOpenHelper(this, DB_NAME)
        assetDatabaseOpenHelper.saveDatabase() // -TO COPY DB FROM ASSETS



        val myDBHelper = DataBaseHelper(this, DB_NAME)
        ItemDataset.mDbHelper = myDBHelper
        //Read data and print
      ItemDataset.TOPIC_ID = 1
        ItemDataset.MENU_ID = 1


        //Initialize the Admob Object and save it so that anybody can access it.
        AdObject.IsOnline=false //--TESTING PURPOSE--DISABLING ADS--/
        AdObject.connectivityManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        AdObject.INTERSTITIAL_ID = AdObject.INTERSTITIAL_TEST_ID
        ItemDataset.APP_DIR = File(filesDir,"bookmarks")
        ItemDataset.APP_DIR.mkdirs()
        AdObject.APPLICATION_ADS_ID = "ca-app-pub-3940256099942544~3347511713"
        AdObject.PACKAGE_NAME = packageName

        /*--------START THE BACKGROUND SERVICE TO CHECK THE INTERNET CONNECTION---*/
        startService(Intent(this,IntentServiceIsOnline::class.java))
//        AdObject.IsOnline = IntentServiceIsOnline().isOnline()


        //Check Internet connection before loading the ad.
        loadBannerWithConnectivityCheck()
        loadItemData()
//        Log.e("CLEAN-AFTER:",ItemDataset.items2[0].menus.toString())

//      val jsondata = applicationContext.assets.open(ItemDataset.APP_JSON_FILE_NAME).bufferedReader().readText()

//        ItemDataset.items = streamingArray(jsondata)
// If the app is in test mode load the test screen else load the splash screen.

        if (AdObject.showAppOrNot()){
        if (AdObject.FRAGMENT_LOADED){
            val prevScreen = AdObject.fragmentsStack.peek()
            when(prevScreen){
                START_SCREEN -> {loadStartScreen()}
                TOPICS -> {loadImageTopics()}
                MENUS-> loadMenus()
                ITEM -> loadItem()
                BOOKMARK_ITEM -> loadBookMarkItem()
                BOOKMARK_MENU -> loadBookMarkMenu()
            }

        }else {
            loadSplashScreen()
        }
        }
        else{
            loadTestModeScreen()
        }

        Thread.setDefaultUncaughtExceptionHandler { t, e -> System.err.println(e.printStackTrace()) }

    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        Log.d("Awww", "App in background")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        Log.d("Yeeey", "App in foreground")
    }

    /*----------------------ON BACK PRESS FOR THE ACTIVITY AND FRAGMENTS-------------------*/
    override fun onBackPressed() {
//        super.onBackPressed()
//        val i = Intent(Intent.ACTION_MAIN)
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

//        if (supportFragmentManager.backStackEntryCount > 0) {
//           // System.err.pritnln(supportFragmentManager.fragments.
//            supportFragmentManager.popBackStack();
//        } else {
//            super.onBackPressed();
//        }
 if (AdObject.fragmentsStack.size > 1) {
           // System.err.pritnln(supportFragmentManager.fragments.
            AdObject.fragmentsStack.pop()
            val prevScreen = AdObject.fragmentsStack.pop() as String

            when(prevScreen){
                START_SCREEN -> {loadStartScreen()}
                TOPICS -> {loadImageTopics()}
                MENUS-> loadMenus()
                ITEM -> loadItem()
                BOOKMARK_ITEM -> loadBookMarkItem()
                BOOKMARK_MENU -> loadBookMarkMenu()
            }


        } else {
            AdObject.FRAGMENT_LOADED = false /*WHEN APP IS GOING INTO BACKGROUND, SET FRAGMENT_LOADED = FALSE*/
            AdObject.SPLASH_CALLED = false
            super.onBackPressed();
        }


//        finish()
    }
    /*--------------------------------------------SCREEN LOADING VIA FRAGMENTS--------------------------------------------------------*/
    /*-----------------------SCREEN 0 - THE SPLASH SCREEN----------------------*/
    override fun loadSplashScreen(){
        if(!isFinishing and !AdObject.SPLASH_CALLED) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, SplashFragment())
                commitAllowingStateLoss()

//                addToBackStack(null)  //DONT KEEP IT IN BACKSTACK
            }


            this.loadBannerWithConnectivityCheck()
          }
    }
    /*-----------------------SCREEN 0 - THE TEST MODE SCREEN----------------------*/
    override fun loadTestModeScreen(){
        if(!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, TestModeFragment())
                commitAllowingStateLoss()
//                addToBackStack(null)  //DONT KEEP IT IN BACKSTACK
            }

            this.loadBannerWithConnectivityCheck()
          }
    }

    /*-----------------------SCREEN 0 - THE MAIN SCREEN----------------------*/
        override fun loadStartScreen(){
            if(!isFinishing) {
                val apply = supportFragmentManager.beginTransaction().apply {
                    replace(R.id.frame_holder, StartScreenFragment())
                    commitAllowingStateLoss()
//                    addToBackStack(null)  //DONT KEEP IT IN BACKSTACK
                }
                AdObject.FRAGMENT_LOADED = true
                AdObject.fragmentsStack.push(START_SCREEN)
                loadBannerWithConnectivityCheck()
              }
    }

    override fun loadPrivacyPolicy() {
        startActivity(Intent(this,PrivacyPolicy::class.java))
    }
    /*-----------------------SCREEN 1 - THE IMAGE TOPICS----------------------*/
    override fun loadImageTopics(){
        if(!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, TopicFragment())
                commitAllowingStateLoss()
            }
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(TOPICS)
            loadBannerWithConnectivityCheck()
          }
    }
    /*---------------------SCREEN 2 - IMAGE MENUS---------------------*/
    override fun loadMenus() {
        if(!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, MenuFragment())
                commitAllowingStateLoss()
            }
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(MENUS)
            loadBannerWithConnectivityCheck()
//            AdObject.setLastLoaded {loadMenus()}
        }
    }

    /*------------------------------SCREEN 3 - THE IMAGE ITEM------------------------*/
    override fun loadItem() {
        if(!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, ItemFragment())
                commitAllowingStateLoss()
            }
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(ITEM)
            loadBannerWithConnectivityCheck()
//            AdObject.setLastLoaded { loadItem() }
        }
    }
    /*------------------------------SCREEN 4 - THE BOOK MARK MENU------------------------*/
    override fun loadBookMarkMenu() {
        if(!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, BookmarkFragment())
                commitAllowingStateLoss()
            }
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(BOOKMARK_MENU)
            loadBannerWithConnectivityCheck()
//            AdObject.setLastLoaded { loadBookMarkMenu() }
        }
    }
    /*------------------------------SCREEN 5 - THE BOOK MARK ITEM------------------------*/
    override fun loadBookMarkItem() {
        if(!isFinishing) {
            val apply = supportFragmentManager.beginTransaction().apply {
                replace(R.id.frame_holder, BookMarkItemFragment())
                commitAllowingStateLoss()
            }
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(BOOKMARK_ITEM)
            loadBannerWithConnectivityCheck()
//            AdObject.setLastLoaded { loadBookMarkItem() }
        }
    }

/*--------------------------------------------SCREEN LOADING VIA FRAGMENTS--------------------------------------------------------*/




    fun parseJSON(array: String):Boolean{

        doAsync {
            val jsonArray = streamingArray(array)

        }
        runOnUiThread {
            alert("JSON parsing done!") {
                yesButton { toast("Yat")
                noButton { toast("haha") }}
            }
        }
        return true
    }


    fun streamingArray(array: String): Collection<Item>  {


        val gson = Gson()

        // Deserialization
        val collectionType = object : TypeToken<Collection<Item>>(){}.type
        val result = gson.fromJson<Collection<Item>>(array, collectionType)


//
//        }
       return result
    }

    fun loadBannerWithConnectivityCheck(){
        if (AdObject.isConnected() and !BANNER_LOADED) {
            findViewById<AdView>(R.id.adBanner).loadAd(AdRequest.Builder().build())
            BANNER_LOADED = true
        }
    }

    fun loadItemData(){
        ItemDataset.items2 = ArrayList<Item>()
        for (ele in 0.rangeTo(Images_Topics.topic_count-1)){
            ItemDataset.items2.add(
                    Item(  topic_title =  Images_Topics.topic_titles[ele],
                           topic_icon =  Images_Topics.topic_icons[ele],
                            menus = getImagesForItem(ele)
                            ))
        }

//        Log.e("DATA",ItemDataset.items2.toString())
    }
    /*Input: INDEX for the Topic
    * Output: Array of the image names and URIS
    * */
    fun getImagesForItem(index_item:Int):ArrayList<String>{


        var menus:ArrayList<String> = ArrayList()
        val topic_size = ceil((images_all.size/Images_Topics.topic_count).toDouble()).toInt()

        val index_from = index_item * topic_size
        var index_to = index_from + topic_size -1
        if (index_to>images_all.size-1){
            index_to = images_all.size-1
        }
        menus.addAll(images_all.subList(index_from,index_to))
        return menus
    }

    /*--------------TO RESTORE THE SCREEN STATE ON RESUME---------------*/
    override fun onResume() {
        super.onResume()
        // If the app is in test mode load the test screen else load the start screen.
        if (AdObject.showAppOrNot()){
            if (AdObject.FRAGMENT_LOADED == true){
                val prevScreen = AdObject.fragmentsStack.peek()
                when(prevScreen){
                    START_SCREEN -> {loadStartScreen()}
                    TOPICS -> {loadImageTopics()}
                    MENUS-> loadMenus()
                    ITEM -> loadItem()
                    BOOKMARK_ITEM -> loadBookMarkItem()
                    BOOKMARK_MENU -> loadBookMarkMenu()
                }

            }else {
                loadSplashScreen()
            }
        }
        else{
            loadTestModeScreen()
        }
    }
}
