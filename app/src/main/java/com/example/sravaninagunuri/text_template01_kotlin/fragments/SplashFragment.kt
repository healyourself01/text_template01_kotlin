package com.example.sravaninagunuri.text_template01_kotlin.fragments


import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.sravaninagunuri.text_template01_kotlin.admob.AdObject
import com.example.sravaninagunuri.text_template01_kotlin.admob.AdmobUtility
import com.example.sravaninagunuri.text_template01_kotlin.AppInterfaces
import com.example.sravaninagunuri.text_template01_kotlin.R


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private lateinit var appInterfaces: AppInterfaces

/**
 * A simple [Fragment] subclass.
 * Use the [TestModeFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class SplashFragment : Fragment() {
    override fun onStart() {
        super.onStart()
//        Toast.makeText(activity,"Splash Fragment Loaded",Toast.LENGTH_SHORT).show()
        AdObject.admob = AdmobUtility(activity, appInterfaces, AdObject.ADS_MODE_PROD, SPLASH_SCREEN = true)
        AdObject.SPLASH_CALLED = true
    }

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.splash_fragment, container, false)
        return v
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        if (activity is AppInterfaces){ appInterfaces = activity }
    }
}
