package com.example.sravaninagunuri.text_template01_kotlin.fragments


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.sravaninagunuri.text_template01_kotlin.*
import com.example.sravaninagunuri.text_template01_kotlin.adapters.CustomItemDecoration
import com.example.sravaninagunuri.text_template01_kotlin.adapters.TopicAdapter
import kotlinx.android.synthetic.main.fragment_topics.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TopicFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class TopicFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var appInterfaces: AppInterfaces
    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        if (activity is AppInterfaces){ appInterfaces = activity }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v:View = inflater.inflate(R.layout.fragment_topics, container, false)
        //RecyclerView Logic
        v.tvHeader.text = "${ItemDataset.topics.size} Categories loaded.."

        val rv =   v.findViewById<RecyclerView>(R.id.rvJokeHeader).apply {
//            layoutManager = GridAutofitLayoutManager(activity as Context, 400, LinearLayoutManager.VERTICAL, false)
            layoutManager = LinearLayoutManager(activity)
            adapter = TopicAdapter(context, activity, appInterfaces)
//            addItemDecoration(BoundaryItemDecoration(context,Color.BLUE,5))
            addItemDecoration(CustomItemDecoration(spacing = 10, includeEdge = false))
        }

//        /*Change the number of columns based on screen orientation*/
//        if (activity!!.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
//            rv.layoutManager=GridAutofitLayoutManager(activity as Context,500,LinearLayoutManager.VERTICAL,false)
//        } else {
//            rv.layoutManager=GridLayoutManager(activity, 1, GridLayoutManager.HORIZONTAL, false)
////            rv.layoutManager=GridAutofitLayoutManager(activity as Context,500,LinearLayoutManager.HORIZONTAL,false)
//        }


        return v
    }





}
