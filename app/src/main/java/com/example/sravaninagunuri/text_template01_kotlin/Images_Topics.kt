package com.example.sravaninagunuri.text_template01_kotlin

object Images_Topics {
    val topic_count = 10 // THE TOTAL TOPIC COUNT
    val topic_titles = arrayOf("Gallery 1","Gallery 2","Gallery 3","Gallery 4","Gallery 5","Gallery 6","Gallery 7","Gallery 8","Gallery 9","Gallery 10") // TOPIC TITLES HERE
    val topic_icons = arrayOf("img001.webp","img002.webp","img003.webp","img004.webp","img005.webp",
            "img006.webp","img007.webp","img008.webp","img009.webp","img010.webp") //TOPIC TITLE ICONS HERE
}