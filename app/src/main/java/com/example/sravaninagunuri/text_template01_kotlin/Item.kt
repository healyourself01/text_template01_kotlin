package com.example.sravaninagunuri.text_template01_kotlin

 data class Item(
         val topic_title:String = "Topic Title",
         val topic_icon:String = "",
         val menus:ArrayList<String>
  )
