package com.example.sravaninagunuri.text_template01_kotlin.adapters

import android.content.Context
import android.net.Uri
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.sravaninagunuri.text_template01_kotlin.*
import com.example.sravaninagunuri.text_template01_kotlin.admob.AdObject
import kotlinx.android.synthetic.main.layout_menu.view.*

class BookMarkAdapter(val ctx:Context, val act:FragmentActivity?,val appInterfaces: AppInterfaces):RecyclerView.Adapter<BookMarkAdapter.ViewHolder>() {


    //GET THE LIST OF IMAGES IN THE BOOKMARK FOLDER
//    val images_bookmarked = ItemDataset.APP_DIR.listFiles()
    val images_bookmarked = ItemDataset.mDbHelper.getAllBookMarks()
    class ViewHolder(val v:View):RecyclerView.ViewHolder(v){

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(ctx).inflate(R.layout.layout_menu,parent,false)

        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
         return images_bookmarked.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        val imgPath = Uri.parse("${ItemDataset.ASSET_URI}${images_bookmarked.elementAt(position)}")
        GlideApp.with(act as FragmentActivity)
                .load(imgPath)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.v.imgMenuIcon)

        holder.v.setOnClickListener(View.OnClickListener {
    //GET THE IMAGE INDEX BASED ON THE IMAGE NAME
            ItemDataset.position_bookmark = position

               AdObject.admob.loadNextScreen {  appInterfaces.loadBookMarkItem() }


        })

    }
}