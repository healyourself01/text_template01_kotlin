package com.example.sravaninagunuri.text_template01_kotlin

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.support.v4.content.FileProvider
import android.widget.Toast
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class AppUtils {

     fun shareImage(bmp: Bitmap,ctx:Context) {

        val share = Intent(Intent.ACTION_SEND)
        share.type = "image/*"

        val bytes = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes)

        val f = File(ItemDataset.APP_DIR,"temp.jpg")
        val uriShare = FileProvider.getUriForFile(ctx as Context,
                "com.mindgame.kotlintemplatefor.images.fileprovider",f)

        try {
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
        } catch (e: IOException) {
            e.printStackTrace()
        }

        share.putExtra(Intent.EXTRA_STREAM, uriShare)
        try {
            ctx.startActivity(Intent.createChooser(share, "Share via"))
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(ctx, "Please Connect To Internet", Toast.LENGTH_LONG)
                    .show()
        }

    }
}