package com.example.sravaninagunuri.text_template01_kotlin.fragments


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.sravaninagunuri.text_template01_kotlin.AppInterfaces
import com.example.sravaninagunuri.text_template01_kotlin.adapters.BookMarkAdapter
import com.example.sravaninagunuri.text_template01_kotlin.GridAutofitLayoutManager
import com.example.sravaninagunuri.text_template01_kotlin.R
import kotlinx.android.synthetic.main.fragment_bookmark.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [BookmarkFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class BookmarkFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var appInterfaces: AppInterfaces

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        if (activity is AppInterfaces){ appInterfaces = activity }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_bookmark, container, false)
        v.tvBookMarkTitle.text="BookMarked Items"
        v.rvBookMarks.apply {
            setHasFixedSize(true)
            layoutManager = GridAutofitLayoutManager(activity as Context, 400, LinearLayoutManager.VERTICAL, false)
            adapter = BookMarkAdapter(context, activity, appInterfaces)

        }
        return  v
    }



}
